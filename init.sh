#!/bin/bash

source ./install.sh

curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
pip3 install neovim
chsh -s /usr/bin/zsh
rm -rf ~/.vim ~/.vimrc ~/.zshrc ~/.tmux ~/.tmux.conf ~/.config/nvim 2> /dev/null
mkdir -p ~/.config ~/.config/nvim

ln -s ~/Development/dotfiles/zshrc ~/.zshrc
ln -s ~/Development/dotfiles/tmux.conf ~/.tmux.conf
ln -s ~/Development/dotfiles/vimrc ~/.config/nvim/init.vim
ln -s ~/Development/dotfiles/Xresources ~/.Xresources
ln -s ~/Development/dotfiles/i3config ~/.config/i3/config
