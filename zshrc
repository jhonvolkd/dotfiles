autoload colors zsh/terminfo
colors
precmd() { print "" }
PS1="⟩"
RPS1="%{$fg[magenta]%}%20<...<%~%<<%{$reset_color%}"
if [ "$TMUX" = "" ]; then tmux; fi
setopt auto_cd

if [[ ! -f ~/.antigen.zsh ]]; then
  curl https://raw.githubusercontent.com/zsh-users/antigen/master/bin/antigen.zsh > ~/.antigen.zsh
fi
source ~/.antigen.zsh

antigen bundle zsh-users/zsh-syntax-highlighting

